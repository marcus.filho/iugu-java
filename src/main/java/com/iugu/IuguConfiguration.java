package com.iugu;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;

import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;

public class IuguConfiguration {

	private final static String URL = "https://api.iugu.com/v1";
	
	private final String tokenId;
	private String proxyHost;
	private int proxyPort;
	private String scheme;

	public IuguConfiguration(String token) {
		tokenId = token;
	}

	public IuguConfiguration (String token, String proxyHost, int proxyPort, String scheme) {
		this(token);
		this.proxyHost = proxyHost;
		this.proxyPort = proxyPort;
		this.scheme = scheme;
	}
	
	public Client getNewClient() {
		Client cl = null;
		if (proxyHost != null) {
			cl = new ResteasyClientBuilder().defaultProxy(proxyHost, proxyPort, scheme).build();
		} else {
			cl = ClientBuilder.newClient();
		}
		cl.register(new Authenticator(tokenId, ""));
		return cl;
	}

	public static String url(String endpoint) {
		return URL + endpoint;
	}

}