package com.iugu.responses;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ListOfPlansResponse {

	@JsonProperty("totalItems")
	private Integer totalItems;
	
	@JsonProperty("items")
	private List<PlanResponse> plans;

	/**
	 * @return the totalItems
	 */
	public Integer getTotalItems() {
		return totalItems;
	}

	/**
	 * @param totalItems the totalItems to set
	 */
	public void setTotalItems(Integer totalItems) {
		this.totalItems = totalItems;
	}

	/**
	 * @return the plans
	 */
	public List<PlanResponse> getPlans() {
		return plans;
	}

	/**
	 * @param plans the plans to set
	 */
	public void setPlans(List<PlanResponse> plans) {
		this.plans = plans;
	}


	
}

