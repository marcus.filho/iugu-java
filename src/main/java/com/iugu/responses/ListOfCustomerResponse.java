package com.iugu.responses;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ListOfCustomerResponse {

	@JsonProperty("totalItems")
	private Integer totalItems;
	
	@JsonProperty("items")
	private List<CustomerResponse> customers;

	/**
	 * @return the totalItems
	 */
	public Integer getTotalItems() {
		return totalItems;
	}

	/**
	 * @param totalItems the totalItems to set
	 */
	public void setTotalItems(Integer totalItems) {
		this.totalItems = totalItems;
	}

	/**
	 * @return the customers
	 */
	public List<CustomerResponse> getCustomers() {
		return customers;
	}

	/**
	 * @param customers the customers to set
	 */
	public void setCustomers(List<CustomerResponse> customers) {
		this.customers = customers;
	}
	
}

