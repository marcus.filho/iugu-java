package com.iugu.responses;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ListOfSubscriptionResponse {

	@JsonProperty("totalItems")
	private Integer totalItems;
	
	@JsonProperty("items")
	private List<SubscriptionResponse> subscriptions;

	/**
	 * @return the totalItems
	 */
	public Integer getTotalItems() {
		return totalItems;
	}

	/**
	 * @param totalItems the totalItems to set
	 */
	public void setTotalItems(Integer totalItems) {
		this.totalItems = totalItems;
	}

	/**
	 * @return the subscriptions
	 */
	public List<SubscriptionResponse> getSubscriptions() {
		return subscriptions;
	}

	/**
	 * @param subscriptions the subscriptions to set
	 */
	public void setSubscriptions(List<SubscriptionResponse> subscriptions) {
		this.subscriptions = subscriptions;
	}
	
	
}

